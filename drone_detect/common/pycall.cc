#include <iostream>
#include <fstream>
#include <stdexcept>
#include <stdlib.h>
#include "pycall.h"


using namespace std;


void PyCall::plotOptFlowGraphs( vector<double> x, vector<double> y )
{
	string fpath("../tmp/");
	fpath = fpath + PY_BAR_PLOTS + ".dat";
	ofstream ofs( fpath.c_str(), ios_base::out | ios_base::trunc );

	size_t i;
	if ( ofs.good() )
	{
		for ( i = 0; i < x.size() - 1; ++i )
		{
			ofs << x[i] << ",";
		}
		ofs << x[i] << endl;
		for ( i = 0; i < y.size() - 1; ++i )
		{
			ofs << y[i] << ",";
		}
		ofs << y[i] << endl;
		ofs.close();
	}
	else
	{
		string msg = string( "[PyCall::plotOptFlowBars] Failed to open data file: " ) + fpath;
		throw invalid_argument( msg );
	}

	string cmd = string("../python/") + PY_BAR_PLOTS + ".py";
	int rc = system( cmd.c_str() );
	if ( rc )
	{
		throw runtime_error( string("[PyCall::plotOptFlowBars] Failed to execute python script: ") + cmd );
	}
}
