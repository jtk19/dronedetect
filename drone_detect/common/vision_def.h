#ifndef __VISION_DEF_H__
#define __VISION_DEF_H__


#define DR_HTTP_HOST				"https://192.168.1.222"
#define DR_DRONEDB_PATH				"/var/www/html/dronedb"
#define DR_DRONEDB_FACE_SUBDIR		"face"
#define DR_DRONEDB_URL_STUB			( std::string("/dronedb/") + string(DR_DRONEDB_FACE_SUBDIR) )
#define DR_DRONEDB_MAX_CLIPS_IN_DIR	1000

#define DR_CAM1_IP					"192.168.10.116"


#define COLOR_BLUE			Scalar( 255, 0, 0)
#define COLOR_GREEN			Scalar( 0, 255, 0)
#define COLOR_RED			Scalar( 0, 0, 255)
#define COLOR_BLACK			Scalar( 0, 0, 0)
#define COLOR_DARK_PINK		Scalar( 102, 0, 204)

#define DR_FRAME_RATE			28

#define DR_ALGO_TIMER_MAX_MS	500


#define DR_SKIP_FRAMES_MOD	2
#define OBJECT_HISTORY_LEN	( DR_FRAME_RATE * 10 )
#define FRAME_HISTORY		( DR_FRAME_RATE / 4 )
#define DIFF_FRAME_NUM		FRAME_HISTORY + 1
#define	DIFF_HISTORY		7
#define BLOB_DET_FRAMES		2


#define	REDUCTION_FACTOR	0.00001

#define NOISE_THRESHOLD		15
#define H_THRESHOLD 		NOISE_THRESHOLD
#define V_THRESHOLD			NOISE_THRESHOLD

#define V_TOP_FRAME_SPACE		116
#define V_BOTTOM_FRAME_SPACE	116
#define H_LEFT_FRAME_SPACE		2
#define H_RIGHT_FRAME_SPACE		2
#define H_FRAME_SPACE	15
#define V_FRAME_SPACE	25
#define VEED_FRAME_VERT_CUTOFF  	1.0000
#define VEED_FRAME_HORIZ_CUTOFF  	0.2000

#define DR_FRAME_DEFAULT_WIDTH		1280
#define DR_FRAME_DEFAULT_HEIGHT		 720



#endif
