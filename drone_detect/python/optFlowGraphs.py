#!/usr/bin/python3

import plotly.plotly as py
import plotly.tools as tls
import tkinter
import matplotlib.pyplot as plt
from matplotlib import interactive
import scipy
from scipy import ndimage
import numpy as np
import sys


data_dir = "../tmp/"
module = "optFlowGraphs"
titles = [ 'Column Optical Flow', 'Row Optical Flow' ]
i = 0

def plotBarGraph( argy ):
    if type(argy) == list or type(argy) == np.ndarray :
        N = len(argy)
        x = range(N)
        width = 1/1.05
        plt.figure()
        myplot = plt.bar(x, argy, width )
        plt.grid()
        global i, titles
        fig = plt.gcf()
        fig.canvas.set_window_title(titles[i])
        i = i + 1
    else:
        print("[optflow::plotBarGraph] Not all arguments passed are lists. Call failed.")


with open( data_dir + module + ".dat" ) as file:
    interactive(True)
    for line in file:
        y = [ float( x.strip() ) for x in (line.strip().split(',')) ]
        plotBarGraph(y)
    interactive(False)
    plt.show()

sys.exit(0)