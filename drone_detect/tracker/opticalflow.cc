#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <deque>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/videoio/videoio_c.h>
#include <opencv2/video/background_segm.hpp>

#include <config.h>
#include <util.h>
#include <pycall.h>
#include <gplot.h>
#include "blobdetector.h"
#include "mobileObject.h"
#include "onlineTracker.h"
#include "opticalflow.h"

#define DR_SKIP_FRAMES_MOD	2
#define OBJECT_HISTORY_LEN	( DR_FRAME_RATE * 10 )
#define FRAME_HISTORY		( DR_FRAME_RATE / 4 )
#define DIFF_FRAME_NUM		FRAME_HISTORY + 1
#define	DIFF_HISTORY		7
#define BLOB_DET_FRAMES		2


#define	REDUCTION_FACTOR	0.00001

#define NOISE_THRESHOLD		15
#define H_THRESHOLD 		NOISE_THRESHOLD
#define V_THRESHOLD			NOISE_THRESHOLD

#define V_TOP_FRAME_SPACE		116
#define V_BOTTOM_FRAME_SPACE	116
#define H_LEFT_FRAME_SPACE		2
#define H_RIGHT_FRAME_SPACE		2
#define H_FRAME_SPACE	15
#define V_FRAME_SPACE	25
#define VEED_FRAME_VERT_CUTOFF  	1.0000
#define VEED_FRAME_HORIZ_CUTOFF  	0.2000



#define TRACK			"Primary Target Track"
#define HIST_WIN		"History Mask"
#define MASK_WIN		"Diff Mask"
#define MOBILE_OBJ		"Motion Detect"


using namespace std;
using namespace cv;


enum Init_T { DR_MANUAL_INIT, DR_OPTFLOW_INIT, DR_TRACKING };


size_t Frame_Rows = DRONE_FRAME_ROWS_DEFAULT;
size_t Frame_Cols = DRONE_FRAME_COLS_DEFAULT;


Drone_Config cfg;
string subdir = cfg.video_subdir[0];

deque<cv::Mat> diff_frames;
cv::Mat diff1_prev;

Init_T init_technique;
bool step_frame = true;
int rc = 0;


Mat dia35 = cv::getStructuringElement( MORPH_RECT, Size(3,5));
Mat dia22 = cv::getStructuringElement( MORPH_RECT, Size(2,2));


cv::Mat Diff;
vector<double> vflow;
vector<double> hflow;
size_t max_v = 0, max_h = 0;
void maxFlowPoint();

deque<MobileObjectFrame> objFrameHist( OBJECT_HISTORY_LEN );
void check_for_neg( Mat m );
size_t detectMaskObjects( Mat &mask, Mat &frame, MobileObjectFrame &fr );

void test();



int main( int argc, char *argv[] )
{
	// test(); return 19;

#ifdef DISPLAY_VIDEO_OPTICALFLOW
	//namedWindow( "TEST", WINDOW_NORMAL | WINDOW_KEEPRATIO );
	//cv::resizeWindow( "TEST", 640, 360 );
	//namedWindow(MASK_WIN, WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO  );
	namedWindow(MOBILE_OBJ, WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO );
	namedWindow(TRACK, WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO );
	//namedWindow(HIST_WIN, WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO );
	//cv::moveWindow( HIST_WIN, 10, 600);
	//cv::resizeWindow( HIST_WIN, 640, 360 );
	//cv::moveWindow( MASK_WIN, 900, 600);
	//cv::resizeWindow( MASK_WIN, 640, 360);
	cv::moveWindow( MOBILE_OBJ, 10, 0);
	cv::resizeWindow( MOBILE_OBJ, (int)(1280 * 0.74), (int)(720*0.74  ) );
	cv::moveWindow( TRACK, 955 , 0);
	cv::resizeWindow( TRACK, (int)(1280 * 0.75), (int)(720*0.75) );
#endif

	string filename;

	// Read all the video files to process.
	vector<string> video_files;

	vector<MobileObject>	vertical_segments;
	vector<MobileObject>	horizontal_segments;


	string write_dir =  string(DRONE_WRITE_DIR) +  "/" + subdir;
	if ( system((string("mkdir -p ")+write_dir).c_str()) == -1 )
	{
		cerr<< "Failed to create the video write directory: "<< write_dir.c_str()<< endl;
	}

	size_t num_video_dir = cfg.video_subdir.size();

	for ( size_t sd = 0; sd < num_video_dir; ++sd )
	{
		subdir = cfg.video_subdir[sd];
		string f_path = string(DRONE_VIDEO_DATA_DIR) + "/" + subdir;
		video_files.clear();
		cfg.getVideoInSubdir( video_files, subdir );

		for ( size_t f = 1; f < video_files.size(); ++f )
		{
			size_t fcount = 0, start_frame;

			step_frame = false;
			if ( f == 0 )
			{
				init_technique = DR_OPTFLOW_INIT;
				start_frame = 21000;
				start_frame = 240;
				step_frame = true;
			}
			else
			{
				init_technique = DR_MANUAL_INIT;
				start_frame = 2;
			}


			OnlineTracker otracker;
			bool otracking = false;
			MobileObjectHistory oHistoryStats;

			GPlot hplt, vplt;

			objFrameHist = deque<MobileObjectFrame>( OBJECT_HISTORY_LEN );

			filename = video_files[f];
			stringstream ss;
			//ss << DRONE_VIDEO_DATA_DIR<< "/"<< subdir<< "/" << filename;
			ss << "rtsp://admin:sts1234@@192.168.10.116:554/";
			cout<< "\n------- Processing: "<< ss.str()<< " -------"<< endl;
			cv::VideoCapture cap( ss.str() );

			char ch; cin >> ch;

			string fname = filename.substr( 0, filename.find('.') );
			string write_track =  write_dir + "/" + fname + "_primary_track.avi";
			cv::VideoWriter track_writer;
			string write_motion =  write_dir + "/" + fname + "_motion_detect.avi";
			cv::VideoWriter motion_writer;
		//	string write_hist =  write_dir + "/" + fname + "_hist"+ common::itoa( FRAME_HISTORY ) + ".avi";
		//	cv::VideoWriter hist_wr;


			cv::Mat f0, f1, f2, fgray, diff, diff1, tmp, tmp1, mask, mhist, mobj, mn;

			bool isWriting = false;
			while ( cap.read(f1) )
			{
				Mat forig = f2 = f1.clone();
				MobileObjectFrame frameObj;
				if ( true )  // init_technique == DR_OPTFLOW_INIT )
				{

					if ( fcount++ <= start_frame )
					{
						if ( fcount == start_frame )
						{
							cout<< " Frame size (c, r): ( " << f1.cols << ", "<< f1.rows<< ") "<< endl;
							Frame_Rows = f1.rows;
							Frame_Cols = f1.cols;

							diff_frames = std::deque<cv::Mat>(DIFF_FRAME_NUM, cv::Mat::zeros( f1.rows, f1.cols, CV_8U ));

							cv::cvtColor( f1, fgray, COLOR_BGR2GRAY );
							cv::absdiff( fgray, f0, diff1_prev );

							isWriting = track_writer.open( write_track, CV_FOURCC('M','J','P','G'), 25, f1.size() );
							isWriting = isWriting &&  motion_writer.open( write_motion, CV_FOURCC('M','J','P','G'), 25, f1.size() );
							//isWriting = isWriting &&  hist_wr.open( write_hist, CV_FOURCC('M','J','P','G'), 25, f1.size() );
							if ( !isWriting )
							{
								cerr<< "Video files [ "<< write_track
										<< " , "<< write_motion<<" ] failed to open for writing!"<< endl;
								// break;
								// proceed without writing video
							}
							else
							{
								cout<< "writing to files: "<< write_track
										<< " & "<< write_motion<< endl;
							}
						}
						mn = f1( cv::Rect(0, 0, f1.cols, V_TOP_FRAME_SPACE ) );
						mn.setTo( Scalar(0,0,0) );
						mn = f1( cv::Rect(0, f1.rows - V_BOTTOM_FRAME_SPACE , f1.cols, V_BOTTOM_FRAME_SPACE ) );
						mn.setTo( Scalar(0,0,0) );
						cvtColor( f1, f0, COLOR_BGR2GRAY );
						#ifdef DISPLAY_VIDEO_OPTICALFLOW
							imshow( TRACK, f1 );
							waitKey(1);
						#endif
						continue;
					} // end start_frame


					f2 = f1.clone();
					mobj = f1.clone();
					cvtColor( f1, fgray, COLOR_BGR2GRAY );

					cv::absdiff( fgray, f0, diff );
					cv::threshold( diff, mask, NOISE_THRESHOLD, 255, cv::THRESH_BINARY );

					diff_frames.push_back( mask.clone() );
					diff_frames.pop_front();

					f0 = fgray.clone();
					diff1_prev = mask.clone();
					Diff = mask.clone();

					if ( fcount % DR_SKIP_FRAMES_MOD != 0 )
					{
						continue;
					}


					/*------- Vertical Optical Flow - TRACKION --------------------------------------------- */

					std::thread max_flow_trd( maxFlowPoint );

					/*int FR = FRAME_HISTORY;
					for ( int i = 0; i < FR; ++i )
					{
						diff = diff + diff_frames[DIFF_FRAME_NUM - i - 2];
					}
					cv::threshold( diff, mn, NOISE_THRESHOLD,  255, cv::THRESH_BINARY );
					cv::dilate( mn, mhist, dia35, Point(-1,-1), 3 );


					vflow = vector<double>( f1.rows, 0.0);
					hflow = vector<double>( f1.cols, 0.0);
					for ( int r = V_TOP_FRAME_SPACE; r < f1.rows - V_BOTTOM_FRAME_SPACE; ++r )
					{
						for ( int c = H_LEFT_FRAME_SPACE; c < f1.cols - H_RIGHT_FRAME_SPACE; ++c )
						{
							//double d = mhist.at<int>( r, c ) > 0 ? 1 : 0;
							double d = mhist.at<int>( r, c ) * REDUCTION_FACTOR;
							vflow[r] += ( d < 0 ? -d : d );

							d = mhist.at<uchar>( r, c )* REDUCTION_FACTOR * 100;
							hflow[c] += d;
						}
					} */

					/*
					//-----------------------------------------------------
					// maximum vertical flow point, max_v
					max_v_val = 0;
					max_v = 0;
					for ( int r = V_TOP_FRAME_SPACE+1; r < f1.rows - V_BOTTOM_FRAME_SPACE-1; ++r )
					{
						double v = vflow[r-1] + vflow[r] + vflow[r+1];
						if ( max_v_val < v )
						{
							max_v_val = v;
							max_v = r;
						}
					}

					bool out = true, hist, ohist;
					MobileObject obj;
					for ( int r = V_TOP_FRAME_SPACE; r < f1.rows - V_BOTTOM_FRAME_SPACE - DIFF_HISTORY; ++r )
					{
						hist = out  &&  vflow[r];
						ohist = !out  && !vflow[r];
						for ( size_t i = 0; i < DIFF_HISTORY; ++i )
						{
							hist = hist  &&  vflow[r+i];
							ohist = ohist &&  !vflow[r+i];
						}
						if ( hist )
						{
							//cv::line( f2, Point( 1, r), Point(f2.cols, r), COLOR_BLUE, 1, LINE_AA );
							obj._top = r;
							out = false;
						}
						if ( ohist )
						{
							//cv::line( f2, Point( 1, r+1), Point(f2.cols, r+1), COLOR_GREEN, 1, LINE_AA );
							obj._btm = r+1;
							out = true;
							vertical_segments.push_back(obj);
						}
					}
					if ( !out )
					{
						int r = f1.rows - V_BOTTOM_FRAME_SPACE;
						//cv::line( f2, Point( 1, r), Point(f2.cols, r), COLOR_GREEN, 1, LINE_AA );
						obj._btm = r;
						out = true;
						vertical_segments.push_back(obj);
					}


					//----------------------------------------------------------------------------------------------
					// Horizontal Optical Flow
					vector<double> hflow( f1.cols, 0.0);	// flow for the whole frame
					for ( size_t x = 0; x < vertical_segments.size(); ++x )
					{
						vector<double> flow( f1.cols, 0.0); // for that segment only
						for ( size_t r = vertical_segments[x]._top; r < vertical_segments[x]._btm; ++r )
						{
							for ( int c = H_LEFT_FRAME_SPACE; c < f2.cols - H_RIGHT_FRAME_SPACE; ++c )
							{
								double d = ( mhist.at<uchar>( r, c ) > 1 ) ? 1 : 0;
								hflow[c] += mhist.at<uchar>( r, c ) * REDUCTION_FACTOR * 100;
								flow[c] += ( d < 0 ? -d : d );
							}
						}

						out = true;
						hist = ohist = false;
						for ( int c = H_LEFT_FRAME_SPACE; c < f1.cols - H_RIGHT_FRAME_SPACE - DIFF_HISTORY; ++c )
						{
							if ( out ) // find left edge in
							{
								hist = out  &&  flow[c];
								for ( size_t i = 0; i < DIFF_HISTORY; ++i )
								{
									hist = hist  &&  flow[c+i];
								}
								if ( hist )
								{
									//cv::line( f2, Point( c, vertical_segments[x]._top), Point(c, vertical_segments[x]._btm), COLOR_RED, 2, LINE_AA  );
									obj._top = vertical_segments[x]._top;
									obj._btm = vertical_segments[x]._btm;
									obj._lft = c;
									out = false;
								}
							}
							else  // in, find right edge out
							{
								ohist = !out  && !flow[c];
								for ( size_t i = 0; i < DIFF_HISTORY; ++i )
								{
									ohist = ohist &&  !flow[c+i];
								}
								if ( ohist )
								{
									obj._rht = c+1;
									out = true;
									frameObj.addOptFlowObject(obj);
								}
							}
						}
						if ( !out )
						{
							int c = f1.cols - H_RIGHT_FRAME_SPACE;
							obj._rht = c;
							frameObj.addOptFlowObject(obj);
							out = true;
						}
					} // end - hflow

					vector<double> hflow( f1.cols, 0.0); // for that segment only
					for ( size_t r = V_TOP_FRAME_SPACE +1; r < V_BOTTOM_FRAME_SPACE; ++r )
					{
						for ( int c = H_LEFT_FRAME_SPACE; c < f2.cols - H_RIGHT_FRAME_SPACE; ++c )
						{
							double d = ( mhist.at<uchar>( r, c ) > 0 ) ? 1 : 0;
							hflow[c] += d;
							//hflow[c] += mhist.at<uchar>( r, c ) * REDUCTION_FACTOR;
							//flow[c] += ( d < 0 ? -d : d );
						}
					}
					cout<< "mhist: "<< mhist.rows<< ", "<< mhist.cols<< endl;
					imshow( "Test19", mhist );
					hplt.plot_bar( "Horizontal Optical Flow", hflow, "-1:1280", "" );

					// Line of maximum horizontal motion
					max_h_val = 0.0;
					max_h = 0;
					for ( int c = H_LEFT_FRAME_SPACE+1; c < f1.cols - H_RIGHT_FRAME_SPACE-1; ++c )
					{
						double h = hflow[c-1] + hflow[c] + hflow[c+1];
						if ( max_h_val < h )
						{
							max_h_val = h;
							max_h = c;
						}
					}
					*/

					//--------------------------------------------------------------------------------------------

					// Secondary algorithm for bounding objects
					/*FR = BLOB_DET_FRAMES;
					diff = mask.clone();
					for ( int i = 0; i < (FR-1); ++i )
					{
						diff = diff + diff_frames[DIFF_FRAME_NUM - i - 2];
					}
					cv::threshold( diff, mn, NOISE_THRESHOLD,  255, cv::THRESH_BINARY ); */
					cv::dilate( mask, mn, dia35, Point(-1,-1), 5 );
					detectMaskObjects( mn, mobj, frameObj );
					//cout<< "Detected "<< frameObj._histMaskObj.size()<< " objects from Historical Mask."<< endl;

					//--------------------------------------------------------------------------------------------

					// mark the axes of maximum motion
					max_flow_trd.join();
					//cout<< "max_v: "<< max_v<< "    max_h: "<< max_h<< endl<< endl;
					cv::line( mobj, Point( 1, max_v), Point( f1.cols -1 , max_v), COLOR_RED, 1, LINE_AA);
					cv::line( mobj, Point( max_h, 1), Point( max_h, f1.rows-1), COLOR_RED, 1, LINE_AA );
					frameObj.addAxes( max_h, max_v );

					//--------------------------------------------------------------------------------------------

					// Plots
					//vplt.plot_bar( "Vertical Optical Flow", vflow, "-1:720", "" );
					//hplt.plot_bar( "Horizontal Optical Flow", hflow, "-1:1280", "" );

					//--------------------------------------------------------------------------------------------
					// Draw objects
					// Outside fame limits
					cv::rectangle( f2, Point( H_LEFT_FRAME_SPACE, V_TOP_FRAME_SPACE),
							 Point( f1.cols - H_RIGHT_FRAME_SPACE, f1.rows - V_BOTTOM_FRAME_SPACE), COLOR_BLUE, 1, LINE_AA );
					cv::rectangle( mobj, Point( H_LEFT_FRAME_SPACE, V_TOP_FRAME_SPACE),
							 Point( f1.cols - H_RIGHT_FRAME_SPACE, f1.rows - V_BOTTOM_FRAME_SPACE), COLOR_BLUE, 1, LINE_AA );


					/*
					//cout<< "Detected "<< frameObj._optFlowObj.size()<< " objects from Optical Flow"<< endl;
					for ( size_t x = 0; x < frameObj._optFlowObj.size(); ++x )
					{
						cv::rectangle( mobj, Point( frameObj._optFlowObj[x]._lft, frameObj._optFlowObj[x]._top),
									Point( frameObj._optFlowObj[x]._rht, frameObj._optFlowObj[x]._btm), COLOR_GREEN, 2 );
					}
					*/

				}

				f2 = forig.clone();
				if ( init_technique == DR_MANUAL_INIT )
				{
					namedWindow( "Manual Select", WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO );
					cv::resizeWindow( "Manual Select", 1280, 720 );
					cv::Rect2d selected = selectROI("Manual Select", forig);
					cv::destroyWindow( "Manual Select");
					if ( selected.area() > 2 )
					{
						init_technique = DR_MANUAL_INIT;
						frameObj._detected_roi = selected;
					}
					else
					{
						init_technique = DR_OPTFLOW_INIT;
						goto DISPLAY;
					}
				}


				//--------------------------------------------------------------------------------------------
				if ( (!otracking)   &&
					 (	(init_technique == DR_MANUAL_INIT) || frameObj.singleDroneDetect() ) )
				{
					cv::rectangle( f2, frameObj._detected_roi, COLOR_BLACK, 1, LINE_AA );
					otracker.init( f2, frameObj._detected_roi );
					otracking = true;
					oHistoryStats.addObject( MobileObject( frameObj._detected_roi ) );
					if (init_technique == DR_MANUAL_INIT)
					{
						init_technique = DR_TRACKING;
						step_frame = false;
					}
				}
				else if ( otracking )
				{
					if ( (init_technique == DR_OPTFLOW_INIT) )
					{
						if ( frameObj.singleDroneDetect() )
						{
							if (oHistoryStats.newObjectGood( MobileObject( frameObj._detected_roi ) ) )
							{
								cv::rectangle( f2, frameObj._detected_roi, COLOR_BLACK, 1, LINE_AA );
								otracker.init( f2, frameObj._detected_roi, false );
								otracking = true;
								oHistoryStats.acceptNewObject( true );
							}
							else
							{
								otracking = true;
								oHistoryStats.acceptNewObject();
								otracking = otracker.track( f2 );
							}
						}
						else // Tracking
						{
							otracking = otracker.track( f2 );
						}
						oHistoryStats.addRegionInfo( frameObj._histMaskObj.size() );
					}
					else if ( init_technique == DR_MANUAL_INIT )
					{
						cv::rectangle( f2, frameObj._detected_roi, COLOR_BLACK, 1, LINE_AA );
						otracker.init( f2, frameObj._detected_roi);
						otracking = true;
						oHistoryStats.clearHistory();
						oHistoryStats.addObject( MobileObject( frameObj._detected_roi ) );
						init_technique = DR_TRACKING;
						step_frame = false;
					}
					else // Tracking
					{
						otracking = otracker.track( f2 );
					}
				}
				objFrameHist.push_front( frameObj );
				objFrameHist.pop_back();

				otracker.consensus();
				//frameObj._trackedObj.push_back( TrackedObject(consensus_location) );

				if ( isWriting )
				{
					track_writer.write(f2);
					motion_writer.write(mobj);

					//cvtColor( mask, tmp, COLOR_GRAY2BGR );
					//mask_wr.write( tmp );

					//cvtColor( mhist, tmp, COLOR_GRAY2BGR );
					//hist_wr.write( tmp );
				}

DISPLAY:
#ifdef DISPLAY_VIDEO_OPTICALFLOW
				imshow( TRACK, f2 );
				imshow( MOBILE_OBJ, mobj );
				/*
				imshow( MASK_WIN, mask );
				imshow( HIST_WIN, mhist );*/

				if ( step_frame )
				{
 					rc = waitKey(0);
				}
				else
				{
					rc = waitKey(1);
				}
				if ( rc == 27 )
				{
					init_technique = DR_MANUAL_INIT;
					step_frame = false;
				}
				else if ( ((char)rc) == ' ')
				{
					step_frame = !step_frame;
				}
				else if ( ((char)rc) == 't')
				{
					init_technique = DR_OPTFLOW_INIT;
					step_frame = false;
					start_frame = fcount + 2;
				}

#endif

			}
			cap.release();
			//cout<< endl;
			if ( isWriting )
			{
				track_writer.release();
				motion_writer.release();
			}
		}
	}

	return 0;
}

void check_for_neg( Mat m )
{
	for ( int r = 0; r < m.rows; ++ r)
	{
		for ( int c = 0; r < m.cols; ++c )
		{
			if ( m.at<double>( r, c) < 0 )
			{
				cout<< r << ","<< c<< " ";
			}
			cout<< endl;
		}
	}


}

size_t detectMaskObjects( Mat &mask, Mat &frame, MobileObjectFrame &fr )
{
	static bool init = true;
	static op::oSimpleBlobDetector::Params params;
	static Ptr<op::oSimpleBlobDetector> detector;

	//vector<KeyPoint> keypoints;
	std::vector<MobileObject> det;
	Mat inv_mask;

	if ( init )
	{
		params.minThreshold = 0;
		params.maxThreshold = 255;

		float max_r = 0.2 * (Frame_Cols - V_TOP_FRAME_SPACE - V_BOTTOM_FRAME_SPACE);
		params.filterByArea = true;
		params.minArea = 4;
		params.maxArea = 2 * M_PI * max_r * max_r;
		//cout<< "Max Area: "<< (unsigned long)params.maxArea<< endl;

		params.filterByConvexity = false;
		//params.minConvexity = 0.01;
		//params.maxConvexity = 1.0;

		params.minDistBetweenBlobs = 34;

		detector = op::oSimpleBlobDetector::create( params );

		init = false;
	}

	cv::bitwise_not( mask, inv_mask);
	detector->detect( inv_mask, det);
	//detector->detect_kp( inv_mask, keypoints);

	for ( size_t i = 0; i < det.size(); ++i )
	{
		//cv::circle( frame, keypoints[i].pt, 0.5 * keypoints[i].size, COLOR_RED, 3, LINE_AA);
		if (!( det[i]._lft <= 1 && det[i]._top <= 1 && det[i]._btm >= (Frame_Rows-1) && det[i]._rht >= (Frame_Cols-1) ) )
		{
			cv::rectangle( frame, cv::Point( det[i]._lft, det[i]._top ),
								cv::Point( det[i]._rht, det[i]._btm ), COLOR_BLUE, 1, LINE_AA);
			fr.addHistMaskObject( det[i] );
		}
	}
	//imshow( "TEST", inv_mask );

	return det.size();
}


void maxFlowPoint()
{
	cv::Mat mn, mhist;
	double max_h_val = 0, max_v_val = 0;

	int FR = FRAME_HISTORY;
	for ( int i = 0; i < FR; ++i )
	{
		Diff = Diff + diff_frames[DIFF_FRAME_NUM - i - 2];
	}
	cv::threshold( Diff, mn, NOISE_THRESHOLD,  255, cv::THRESH_BINARY );
	cv::dilate( mn, mhist, dia35, Point(-1,-1), 3 );

	mn = mhist( cv::Rect(0, 0, mhist.cols, V_TOP_FRAME_SPACE ) );
	mn.setTo( Scalar(0,0,0) );
	mn = mhist( cv::Rect(0, mhist.rows - V_BOTTOM_FRAME_SPACE , mhist.cols, V_BOTTOM_FRAME_SPACE ) );
	mn.setTo( Scalar(0,0,0) );


	vflow = vector<double>( Frame_Rows, 0.0);
	hflow = vector<double>( Frame_Cols, 0.0);
	for ( size_t r = V_TOP_FRAME_SPACE; r < Frame_Rows - V_BOTTOM_FRAME_SPACE; ++r )
	{
		for ( size_t c = H_LEFT_FRAME_SPACE; c < Frame_Cols - H_RIGHT_FRAME_SPACE; ++c )
		{
			//double d = mhist.at<int>( r, c ) > 0 ? 1 : 0;
			double d = mhist.at<int>( r, c ) * REDUCTION_FACTOR;
			vflow[r] += ( d < 0 ? -d : d );

			d = mhist.at<uchar>( r, c )* REDUCTION_FACTOR * 100;
			hflow[c] += d;
		}
	}

	max_v = 0;
	for ( size_t r = V_TOP_FRAME_SPACE+1; r < Frame_Rows - V_BOTTOM_FRAME_SPACE-1; ++r )
	{
		double v = vflow[r-1] + vflow[r] + vflow[r+1];
		if ( max_v_val < v )
		{
			max_v_val = v;
			max_v = r;
		}
	}

	max_h_val = 0.0;
	max_h = 0;
	for ( size_t c = H_LEFT_FRAME_SPACE+1; c < Frame_Cols - H_RIGHT_FRAME_SPACE-1; ++c )
	{
		double h = hflow[c-1] + hflow[c] + hflow[c+1];
		if ( max_h_val < h )
		{
			max_h_val = h;
			max_h = c;
		}
	}

}

void test()
{

	cv::Mat m;
	cv::Mat m1( 10, 10, 0);

	cout<< "Mat m0 empty ? "<< m.empty() << endl;
	cout<< "Mat m1 empty ? "<< m1.empty() << endl;

}

