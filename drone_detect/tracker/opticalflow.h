#ifndef __OPTICALFLOW_H__
#define __OPTICALFLOW_H__


#define COLOR_BLUE			Scalar( 255, 0, 0)
#define COLOR_GREEN			Scalar( 0, 255, 0)
#define COLOR_RED			Scalar( 0, 0, 255)
#define COLOR_BLACK			Scalar( 0, 0, 0)
#define COLOR_DARK_PINK		Scalar( 102, 0, 204)

#define DR_FRAME_RATE			28

#define DR_ALGO_TIMER_MAX_MS	500


extern size_t Frame_Rows;
extern size_t Frame_Cols;


#endif
