
#include <stdlib.h>
#include <opencv2/features2d.hpp>

#include "mobileObject.h"


using namespace cv;
using namespace std;


namespace op
{

	struct Center
    {
	    Point2d location;
	    double radius;
	    double confidence;

	    MobileObject ob;
    };

	class oSimpleBlobDetector : public Feature2D
	{
	public:
	  struct Params
	  {
		  Params();
		  float thresholdStep;
		  float minThreshold;
		  float maxThreshold;
		  size_t minRepeatability;
		  float minDistBetweenBlobs;

		  bool filterByColor;
		  unsigned char blobColor;

		  bool filterByArea;
		  float minArea, maxArea;

		  bool filterByCircularity;
		  float minCircularity, maxCircularity;

		  bool filterByInertia;
		  float minInertiaRatio, maxInertiaRatio;

		  bool filterByConvexity;
		  float minConvexity, maxConvexity;

	  };

	 oSimpleBlobDetector(const op::oSimpleBlobDetector::Params&);

	 static Ptr<oSimpleBlobDetector>
	 create(const oSimpleBlobDetector::Params &parameters = oSimpleBlobDetector::Params());

	 void detect_kp( cv::Mat image, std::vector<KeyPoint>& keypoints, cv::Mat mask=cv::Mat() );
	 void detect( cv::Mat image, std::vector<MobileObject> &detObj );

	 void findblobs( cv::Mat image, cv::Mat binaryImage, std::vector<Center> &centers ) const;

	private:
	 Params params;

	 vector<MobileObject> _detObjects;

	};

}
