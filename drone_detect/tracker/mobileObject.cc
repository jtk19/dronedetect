
#include <iostream>
#include <functional>
#include "mobileObject.h"


using namespace std;
using namespace cv;


size_t MobileObject::objectId = 0;
size_t TrackedObject::id_index = 0;


bool MobileObjectFrame::singleDroneDetect()
{
	bool det = true;

	//cout<< "obj # optflow: "<<  _optFlowObj.size()<< " hist: "<< _histMaskObj.size()<< endl;
	//det = ( _optFlowObj.size() == 1 ) && ( _histMaskObj.size() == 1 );
	det = ( ( _histMaskObj.size() == 1 ) &&
			_histMaskObj[0].contains( _maxMotionAxes ) );

	/*
	if ( det )
	{
		long overlap = ( _optFlowObj[0].getRect()  &  _histMaskObj[0].getRect() ).area();
		float op = ( overlap / _histMaskObj[0].area() );
		det = ( overlap ==  _histMaskObj[0].area()  ||
				overlap ==  _optFlowObj[0].area()  ||
				( overlap  > 0.1 * _histMaskObj[0].area() )  );
		// i.e there is reasonable agreement between the 2 predictions
		//cout << "  Good overlap? "<< ( det ? "true" : "false" )
		//		<< "   "<< overlap << " / "<< _optFlowObj[0].getArea()<< endl;
	}

	det = det && ( _optFlowObj[0].contains( _maxMotionAxes )
			      || _histMaskObj[0].contains( _maxMotionAxes ) );
	//cout << "  Contains axes? "<< ( det ? "true" : "false" )<< endl;
	 *
	 */

	_detected = det;
	if ( det )
	{
		/*_detected_roi = cv::Rect( _optFlowObj[0]._lft, _optFlowObj[0]._top,
				_optFlowObj[0]._rht - _optFlowObj[0]._lft,
				_optFlowObj[0]._btm - _optFlowObj[0]._top ); */
		_detected_roi = _histMaskObj[0].getRect();

		++MobileObject::objectId;
		_detected_name = string( "Drone_1" );
	}

	return det;
}

void MobileObjectHistory::addObject( const MobileObject &obj )
{
	_obj.push_front(obj);
	if ( _obj.size() >  DR_STATS_HISTORY_LEN )
	{
		_obj.pop_back();
	}
}


void MobileObjectHistory::acceptNewObject(bool forceAccpet)
{
	if ( forceAccpet  || isRegionGood() )
	{
		_obj.push_front(_newObj);
		if ( _obj.size() > DR_STATS_HISTORY_LEN )
		{
			_obj.pop_back();
		}
	}
}


bool MobileObjectHistory::newObjectGood() const
{
	float w = 0.6, tolerance = 0.2;
	float ref_area, diff;
	long med;

	if ( !isRegionGood() )
	{
		return false;
	}

	if ( _obj.size() == 0 )
	{
		return true;
	}

	bool b = (_newObj.width() > _newObj.height() );

	if ( b )
	{
		vector<long> area;
		size_t len = ( _obj.size() < 5 ) ? _obj.size() : 5 ;
		std::transform( _obj.begin(), _obj.begin() + len, std::back_inserter(area),
								[](const MobileObject &o) { return o.area(); } );
		std::sort( area.begin(), area.end() );
		med = area[ (size_t)( 0.5 * area.size() ) ];

		ref_area = w * _obj[0].area() + ( 1 - w ) * med;

		//ref_area = _obj[0].area();
		diff = cv::abs( _newObj.area() - ref_area );

		diff /= ref_area;
	}
	b = ( b  && (diff < tolerance) );

	if (b)
	{
		double dist = cv::norm( _obj[0].centre() - _newObj.centre() );
		b = ( dist < (2 * _obj[0].width() ) );
	}

	return b;
}


bool MobileObjectHistory::newObjectGood( const MobileObject &obj )
{
	_newObj = obj;
	return newObjectGood();
}

void MobileObjectHistory::addRegionInfo( size_t mobileRegions )
{
	_objectSum += mobileRegions;
	_objectCount.push_back(mobileRegions);
	if ( _objectCount.size() > DR_STATS_HISTORY_LEN )
	{
		_objectSum -= _objectCount[0];
		_objectCount.pop_front();
	}
}


bool MobileObjectHistory::isRegionGood() const
{
	if ( _objectCount.size() == 0 )
	{
		return true;
	}
	float avg = _objectSum / _objectCount.size();
	return ( avg < 3.0 );
}

