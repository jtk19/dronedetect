#include <iterator>
#include <limits>
#include <opencv2/imgproc.hpp>

#include <vision_def.h>
#include "blobdetector.h"

//#define DEBUG_BLOB_DETECTOR


namespace op
{


/*
*  SimpleBlobDetector
*/
oSimpleBlobDetector::Params::Params()
{
    thresholdStep = 10;
    minThreshold = 50;
    maxThreshold = 220;
    minRepeatability = 2;
    minDistBetweenBlobs = 10;

    filterByColor = true;
    blobColor = 0;

    filterByArea = true;
    minArea = 25;
    maxArea = 5000;

    filterByCircularity = false;
    minCircularity = 0.8f;
    maxCircularity = std::numeric_limits<float>::max();

    filterByInertia = true;
    //minInertiaRatio = 0.6;
    minInertiaRatio = 0.1f;
    maxInertiaRatio = std::numeric_limits<float>::max();

    filterByConvexity = true;
    //minConvexity = 0.8;
    minConvexity = 0.95f;
    maxConvexity = std::numeric_limits<float>::max();
}


void oSimpleBlobDetector::findblobs( cv::Mat image, cv::Mat binaryImage, std::vector<Center> &centers ) const
{
    //CV_INSTRUMENT_REGION();
    centers.clear();

    std::vector < std::vector<Point> > contours;
    cv::findContours(binaryImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

    for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
    {
        Center center;
        center.confidence = 1;
        Moments moms = moments(contours[contourIdx]);
        if (params.filterByArea)
        {
            double area = moms.m00;
            if (area < params.minArea || area >= params.maxArea)
                continue;
        }

        if (params.filterByCircularity)
        {
            double area = moms.m00;
            double perimeter = arcLength(contours[contourIdx], true);
            double ratio = 4 * CV_PI * area / (perimeter * perimeter);
            if (ratio < params.minCircularity || ratio >= params.maxCircularity)
                continue;
        }

        if (params.filterByInertia)
        {
            double denominator = std::sqrt(std::pow(2 * moms.mu11, 2) + std::pow(moms.mu20 - moms.mu02, 2));
            const double eps = 1e-2;
            double ratio;
            if (denominator > eps)
            {
                double cosmin = (moms.mu20 - moms.mu02) / denominator;
                double sinmin = 2 * moms.mu11 / denominator;
                double cosmax = -cosmin;
                double sinmax = -sinmin;

                double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
                double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
                ratio = imin / imax;
            }
            else
            {
                ratio = 1;
            }

            if (ratio < params.minInertiaRatio || ratio >= params.maxInertiaRatio)
                continue;

            center.confidence = ratio * ratio;
        }

        if (params.filterByConvexity)
        {
            std::vector < Point > hull;
            convexHull(contours[contourIdx], hull);
            double area = contourArea(contours[contourIdx]);
            double hullArea = contourArea(hull);
            if (fabs(hullArea) < DBL_EPSILON)
                continue;
            double ratio = area / hullArea;
            if (ratio < params.minConvexity || ratio >= params.maxConvexity)
                continue;
        }

        if(moms.m00 == 0.0)
            continue;
        center.location = Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);

        if (params.filterByColor)
        {
            if (binaryImage.at<uchar> (cvRound(center.location.y), cvRound(center.location.x)) != params.blobColor)
                continue;
        }

        MobileObject o;
        //compute blob radius
        {
        	o._top = o._lft = SIZE_MAX;
            std::vector<double> dists;
            for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
            {
                Point2d pt = contours[contourIdx][pointIdx];
                dists.push_back(norm(center.location - pt));
                if ( pt.x < o._lft )
                {
                	o._lft = pt.x;
                }
                if ( pt.x > o._rht )
                {
                	o._rht = pt.x;
                }
                if ( pt.y < o._top )
                {
                	o._top = pt.y;
                }
                if ( pt.y > o._btm )
                {
                	o._btm = pt.y;
                }
            }
            std::sort(dists.begin(), dists.end());
            center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
            center.ob = o;
        }

        centers.push_back(center);
    }

}


void oSimpleBlobDetector::detect_kp( cv::Mat image, std::vector<cv::KeyPoint>& keypoints, cv::Mat mask)
{

    keypoints.clear();
    CV_Assert(params.minRepeatability != 0);
    cv::Mat grayscaleImage;
    if (image.channels() == 3 || image.channels() == 4)
        cvtColor(image, grayscaleImage, COLOR_BGR2GRAY);
    else
        grayscaleImage = image;

    if (grayscaleImage.type() != CV_8UC1) {
        CV_Error(Error::StsUnsupportedFormat, "Blob detector only supports 8-bit images!");
    }

    std::vector< std::vector<Center> > centers;
    for (double thresh = params.minThreshold; thresh < params.maxThreshold; thresh += params.thresholdStep)
    {
        Mat binarizedImage;
        threshold( grayscaleImage, binarizedImage, thresh, 255, THRESH_BINARY);

        std::vector<Center> curCenters;
        oSimpleBlobDetector::findblobs( grayscaleImage, binarizedImage, curCenters );
        std::vector < std::vector<Center> > newCenters;
        for (size_t i = 0; i < curCenters.size(); i++)
        {
            bool isNew = true;
            for (size_t j = 0; j < centers.size(); j++)
            {
                double dist = norm(centers[j][ centers[j].size() / 2 ].location - curCenters[i].location);
                isNew = dist >= params.minDistBetweenBlobs && dist >= centers[j][ centers[j].size() / 2 ].radius && dist >= curCenters[i].radius;
                if (!isNew)
                {
                    centers[j].push_back(curCenters[i]);

                    size_t k = centers[j].size() - 1;
                    while( k > 0 && centers[j][k].radius < centers[j][k-1].radius )
                    {
                        centers[j][k] = centers[j][k-1];
                        k--;
                    }
                    centers[j][k] = curCenters[i];

                    break;
                }
            }
            if (isNew)
                newCenters.push_back(std::vector<Center> (1, curCenters[i]));
        }
        std::copy(newCenters.begin(), newCenters.end(), std::back_inserter(centers));
    }

    for (size_t i = 0; i < centers.size(); i++)
    {
        if (centers[i].size() < params.minRepeatability)
            continue;
        Point2d sumPoint(0, 0);
        double normalizer = 0;
        for (size_t j = 0; j < centers[i].size(); j++)
        {
            sumPoint += centers[i][j].confidence * centers[i][j].location;
            normalizer += centers[i][j].confidence;
        }
        sumPoint *= (1. / normalizer);
        KeyPoint kpt(sumPoint, (float)(centers[i][centers[i].size() / 2].radius) * 2.0f);
        keypoints.push_back(kpt);
    }

    if (!mask.empty())
    {
        KeyPointsFilter::runByPixelsMask(keypoints, mask);
    }
}


void op::oSimpleBlobDetector::detect( cv::Mat image, std::vector<MobileObject>& detObj )
{
	vector<cv::KeyPoint> keypoints;

	keypoints.clear();
	CV_Assert(params.minRepeatability != 0);
	Mat grayscaleImage;
	if (image.channels() == 3 || image.channels() == 4)
		cvtColor(image, grayscaleImage, COLOR_BGR2GRAY);
	else
		grayscaleImage = image;

	if (grayscaleImage.type() != CV_8UC1)
	{
		CV_Error(Error::StsUnsupportedFormat, "Blob detector only supports 8-bit images!");
	}

	std::vector< std::vector<Center> > centers;
	for (double thresh = params.minThreshold; thresh < params.maxThreshold; thresh += params.thresholdStep)
	{
		Mat binarizedImage;
		threshold(grayscaleImage, binarizedImage, thresh, 255, THRESH_BINARY);

		std::vector < Center > curCenters;
		oSimpleBlobDetector::findblobs(grayscaleImage, binarizedImage, curCenters );
		std::vector < std::vector<Center> > newCenters;
		for (size_t i = 0; i < curCenters.size(); i++)
		{
			bool isNew = true;
			for (size_t j = 0; j < centers.size(); j++)
			{
				double dist = norm(centers[j][ centers[j].size() / 2 ].location - curCenters[i].location);
				isNew = dist >= params.minDistBetweenBlobs && dist >= centers[j][ centers[j].size() / 2 ].radius && dist >= curCenters[i].radius;
				if (!isNew)
				{
					centers[j].push_back(curCenters[i]);

					size_t k = centers[j].size() - 1;
					while( k > 0 && centers[j][k].radius < centers[j][k-1].radius )
					{
						centers[j][k] = centers[j][k-1];
						k--;
					}
					centers[j][k] = curCenters[i];

					break;
				}
			}
			if (isNew)
				newCenters.push_back(std::vector<Center> (1, curCenters[i]));
		}
		std::copy(newCenters.begin(), newCenters.end(), std::back_inserter(centers));
	}

	vector<MobileObject> det( centers.size(), MobileObject( image.rows, 0, image.cols, 0 ));

	int not_good = 0;
	for (size_t i = 0; i < centers.size(); i++)
	{
		if (centers[i].size() < params.minRepeatability)
		{
			++not_good;
			continue;
		}
		Point2d sumPoint(0, 0);
		double normalizer = 0;
		for (size_t j = 0; j < centers[i].size(); j++)
		{
			sumPoint += centers[i][j].confidence * centers[i][j].location;
			normalizer += centers[i][j].confidence;

			MobileObject ob = centers[i][j].ob;
			if ( det[i]._top > ob._top )
			{
				det[i]._top = ob._top;
			}
			if ( det[i]._btm < ob._btm )
			{
				det[i]._btm = ob._btm;
			}
			if ( det[i]._lft > ob._lft )
			{
				det[i]._lft = ob._lft;
			}
			if ( det[i]._rht < ob._rht )
			{
				det[i]._rht = ob._rht;
			}
		}
		sumPoint *= (1. / normalizer);
		KeyPoint kpt(sumPoint, (float)(centers[i][centers[i].size() / 2].radius) * 2.0f);
		keypoints.push_back(kpt);
	}

	detObj.clear();
	std::copy( det.begin(), det.end() - not_good, std::back_inserter(detObj) );
}


op::oSimpleBlobDetector::oSimpleBlobDetector(const op::oSimpleBlobDetector::Params&p)
  : params(p)
{
}

Ptr<oSimpleBlobDetector> oSimpleBlobDetector::create(const oSimpleBlobDetector::Params& params)
{
    return makePtr<oSimpleBlobDetector>(params);
}


}
