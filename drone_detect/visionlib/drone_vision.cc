
#include <stdexcept>
#include <sstream>
#include <stdlib.h>

#include <vision_def.h>
#include <blobdetector.h>
#include <drone_vision.h>

using namespace std;



namespace Drone
{

size_t Motion_Detect::detectMaskObjects( Mat mask, MobileObjectFrame &fr, Mat frame, bool setBorder )
{
	static bool init = true;
	static op::oSimpleBlobDetector::Params params;
	static Ptr<op::oSimpleBlobDetector> detector;

	//vector<KeyPoint> keypoints;
	std::vector<MobileObject> det;
	Mat inv_mask;

	if ( init )
	{
		params.minThreshold = 0;
		params.maxThreshold = 255;

		float max_r = 0.2 * mask.cols;
		params.filterByArea = true;
		params.minArea = 4;
		params.maxArea = 2 * M_PI * max_r * max_r;
		//cout<< "Max Area: "<< (unsigned long)params.maxArea<< endl;

		params.filterByConvexity = false;
		//params.minConvexity = 0.01;
		//params.maxConvexity = 1.0;

		params.minDistBetweenBlobs = 34;

		detector = op::oSimpleBlobDetector::create( params );

		init = false;
	}

	if ( setBorder )
	{
		Mat mn;
		mn = mask( cv::Rect(0, 0, mask.cols, V_TOP_FRAME_SPACE ) );
		mn.setTo( Scalar(0,0,0) );
		mn = mask( cv::Rect(0, mask.rows - V_BOTTOM_FRAME_SPACE, mask.cols, V_BOTTOM_FRAME_SPACE ) );
		mn.setTo( Scalar(0,0,0) );
	}

	cv::bitwise_not( mask, inv_mask);
	detector->detect( inv_mask, det);
	//detector->detect_kp( inv_mask, keypoints);

	for ( size_t i = 0; i < det.size(); ++i )
	{
		//cv::circle( frame, keypoints[i].pt, 0.5 * keypoints[i].size, COLOR_RED, 3, LINE_AA);
		if (!( det[i]._lft <= 1 && det[i]._top <= 1 &&
				det[i]._btm >= (mask.rows-1) &&
				det[i]._rht >= (mask.cols-1) ) )
		{
			if ( !(frame.empty()) )
			{
				cv::rectangle( frame, cv::Point( det[i]._lft, det[i]._top ),
								cv::Point( det[i]._rht, det[i]._btm ), COLOR_BLUE, 1, LINE_AA);
			}
			fr.addHistMaskObject( det[i] );
		}
	}
	//imshow( "TEST", inv_mask );

	return det.size();
}


size_t Motion_Detect::detectMobileObjects( Mat fgray, Mat fgray_prev, MobileObjectFrame &fr_obj,
												Mat frame, bool setBorder )
{
	static Mat dia35 = cv::getStructuringElement( MORPH_RECT, Size(3,5));

	size_t rtn;
	cv::Mat tmp, mask;

	cv::absdiff( fgray, fgray_prev, tmp );
	cv::threshold( tmp, mask, NOISE_THRESHOLD, 255, cv::THRESH_BINARY );
	cv::dilate( mask, tmp, dia35, Point(-1,-1), 5 );

	rtn = detectMaskObjects( mask, fr_obj, frame, setBorder );

	return rtn;
}

cv::Rect Motion_Detect::getNearestMobileObject( size_t x, size_t y, string camStreamUrl )
{
	Mat f0, f1;

	//ss << "rtsp://admin:sts1234\@@192.168.10.115:554/";
	cv::VideoCapture cap( camStreamUrl );
	if ( !cap.read(f0) )
	{
		throw runtime_error( string("[Motion_Detect::getNearestMobileObject] Failed to open camera RTSP url: ")
								+ camStreamUrl );
	}
	while ( cap.read(f1) )
	{

	}

	return Rect( 0, 0, 100, 100);
}


cv::Mat Motion_Detect::getCamClip( string camIp, string camUsr, string camPwd )
{
	Mat f1;
	stringstream ss;
	ss << "rtsp://" << camUsr << ":" << camPwd
	   << "@" << camIp << string(":") << DR_CAM_RTSP_PORT;
#ifdef DEBUG
	cout<< "Connecting to camera ["<< ss.str()<< "]"<< endl;
#endif
	cv::VideoCapture cap( ss.str() );

	if ( cap.read(f1) )
	{
		//namedWindow( "Manual Select", WINDOW_GUI_NORMAL | WINDOW_KEEPRATIO );
		//cv::resizeWindow( "Manual Select", DR_FRAME_DEFAULT_WIDTH, DR_FRAME_DEFAULT_HEIGHT );
		//cv::Rect2d selected = selectROI("Manual Select", f1);
		//cv::destroyWindow( "Manual Select");
		int px = rand() % 200 + 1;
		int sign = rand() % 10;
		if ( sign < 5)
		{
			px = -px;
		}
		int yy = 55- + px;
		yy = ( yy + 300 ) > f1.cols ? f1.cols - 300 : yy;

		cv::Rect2d selected = cv::Rect( 1000, 520 + px, 400, 300);
		if ( selected.area() > 2 )
		{
			return f1(selected);
		}
		else
		{
			cerr<< "[Motion_Detect::getCamClip(1)] Manual select failed."<< endl;
			cv::Mat empty;
			return empty;
		}
	}
	else
	{
		cerr<< "[Motion_Detect::getCamClip] Failed to open & read camera: "<< ss.str()<< "   "<< endl;
		cv::Mat empty;
		return empty;
	}

	return f1;
}

cv::Mat Motion_Detect::getCamClip( string camIp, string camUsr, string camPwd, int x, int y, int w, int h )
{
	Mat f1;
	stringstream ss;
	ss << "rtsp://" << camUsr << ":" << camPwd
	   << "@" << camIp << string(":") << DR_CAM_RTSP_PORT;
#ifdef DEBUG
	cout<< "Connecting to camera ["<< ss.str()<< "]"<< endl;
#endif
	cv::VideoCapture cap( ss.str() );

	if ( cap.read(f1) )
	{
		if ( x > f1.cols  ||  y > f1.rows )
		{
			cerr<< "[Motion_Detect::getCamClip(2)] (x,y)=("<< x<< ","<< y<< ") "
				<< "outside the frame of size ( "<< f1.cols << ", "<< f1.rows<< " )"<< endl;
		}
		else
		{
			if ( (x+w) > f1.cols )
			{
				w = f1.cols - x;
			}
			if ( (y+h) > f1.rows )
			{
				h = f1.rows - y;
			}
			return f1( cv::Rect( x, y, w, h) );
		}
	}
	else
	{
		cerr<< "[Motion_Detect::getCamClip(2)] Failed to open & read camera."<< endl;
		f1 = Mat();
	}

	return f1;
}


} // namespace
