#-------------------------------------------------------------
# Makefile for 90-degree orientation program
#-------------------------------------------------------------

.DEFAULT_GOAL := this

ifneq ($(BUILD),"RELEASE")
        export BUILD = "DEBUG"
endif

MODULE := libdrone_vision


COMMON_BUILD := ../build
COMMON_SRC := .
COMMON_INC := ../inc
COMMON_CFG := ../cfg
COMMON_INCLUDE := ../include
COMMON_LIB := ../lib


curr_dir := $(shell pwd)

THIS_BUILD := $(COMMON_BUILD)/visionlib_build
TARGET = $(COMMON_BUILD)/$(MODULE)
TEST = $(COMMON_BUILD)/$(MODULE)_test

BUILDDIRS = common visionlib


CC = g++
 
INCLUDES = -I$(COMMON_INC) -I/usr/local/include -I/usr/include/python3.6m -I/usr/local/include/opencv4

LIBS = -L/usr/local/lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/boost -L$(COMMON_BUILD) -ldrone_vision -pthread \
		-lopencv_highgui -lopencv_core -lopencv_videoio -lopencv_video \
		-lopencv_imgproc -lopencv_features2d -lopencv_tracking \
		-lboost_iostreams -lboost_system -lboost_filesystem

SUBDIRS = 

EXTDIRS = ../common 

SOURCES = $(shell echo *.cc)

HEADERS = $(shell echo *.h)

OBJECTS = $(SOURCES:%.cc=$(THIS_BUILD)/%.o)


ALL_OBJECTS1 = $(addprefix $(COMMON_BUILD)/, $(BUILDDIRS) )
ALL_OBJECTS = $(addsuffix _build/*.o, $(ALL_OBJECTS1) )



CFLAGS = -fPIC -g -std=c++14

LDFLAGS = -shared

ifeq ($(BUILD),"RELEASE")
	CFLAGS += -O2
	LDFLAGS += -s
else ## DEBUG
	CFLAGS += -g -DDEBUG
endif


$(OBJECTS): $(THIS_BUILD)/%.o : %.cc %.h
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@  
	
	
.PHONY: target_lib
target_lib: $(TARGET).a $(TARGET).so
$(TARGET).a : $(OBJECTS)
	ar cr $@ $^
	ranlib $@
	
$(TARGET).so : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@ $(LDFLAGS)
	
$(TEST): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ test.cpp $(ALL_OBJECTS) $(LIBS)	
		
	
define fullBuild
	@# build
	cp $(HEADERS) $(COMMON_INCLUDE)
	cp $(HEADERS) $(COMMON_INC)
	@echo "Building $(curr_dir) - full build "
	mkdir -p $(THIS_BUILD) $(COMMON_INCLUDE)
	$(MAKE) target_lib
	@# post-build steps
	cp $(TARGET).so $(TARGET).a $(COMMON_LIB)
endef

define extbuild
	cp $(HEADERS) $(COMMON_INCLUDE)
	cp $(HEADERS) $(COMMON_INC)
	@# build
	@echo "Building $(curr_dir) - ext build "
	@for extdir in $(EXTDIRS); do \
	    echo "Making all in $$extdir"; \
	    $(MAKE) -C $$extdir all;\
	done
	mkdir -p $(THIS_BUILD) $(COMMON_INCLUDE)
	$(MAKE) target_lib
	@# post-build steps
	cp $(TARGET).so $(TARGET).a $(COMMON_LIB)
endef


this: tree
tree:	
	$(call fullBuild)
	
	
all:
	$(call extbuild)
	
	
test:	
	$(call fullBuild)
	$(MAKE) $(TEST)
	

clean:
	touch $(TEST)
	rm -rf $(THIS_BUILD) $(TARGET).a $(TARGET).so touch $(TEST)
	$(shell cd $(COMMON_INCLUDE); touch $(HEADERS); rm -f $(HEADERS) )
	$(shell cd $(COMMON_INC); touch $(HEADERS); rm -f $(HEADERS))
	$(shell cd $(COMMON_LIB); touch $(MODULE).so $(MODULE).a; rm -f $(MODULE).so $(MODULE).a)
	
	
	
	
