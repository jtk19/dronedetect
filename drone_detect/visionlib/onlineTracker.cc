
#include <time.h>
#include <thread>

#include <vision_def.h>
#include "onlineTracker.h"

#define MOTION_DETECT	"Motion Detect"
#define ALGO_TURN_OFF_FRAME_COUNT	( DR_FRAME_RATE / 3)


const char *OnlineTracker::algo_str[] = { "KCF", "MIL", "GOTURN CNN", "UNKNOWN" };
const cv::Scalar OnlineTracker::col[] = { COLOR_GREEN, COLOR_BLUE, COLOR_DARK_PINK };

Ptr<Tracker> OnlineTracker::miltr;
Ptr<Tracker> OnlineTracker::kcftr;
Ptr<Tracker> OnlineTracker::cnntr;

static bool turned_off = false;
static size_t turned_off_frame_count = 99;


void runTracker( OnlineTracker::Algo_T algo, OnlineTracker *tr_obj )
{
	clock_t timer;
	double duration;
	Ptr<Tracker> tr;

	if ( turned_off  &&  (algo == OnlineTracker::ALGO_CNN ) )
	{
		if (++turned_off_frame_count < ALGO_TURN_OFF_FRAME_COUNT )
		{
			return;
		}
		else
		{
			turned_off = false;
			cout<< "  Turning on "<< OnlineTracker::algo_str[algo]<< " again."<< endl;
		}
	}

	switch (algo)
	{
	case OnlineTracker::ALGO_KCF : tr = OnlineTracker::kcftr; break;
	case OnlineTracker::ALGO_MIL : tr = OnlineTracker::miltr; break;
	case OnlineTracker::ALGO_CNN : tr = OnlineTracker::cnntr; break;
	default:
		std::cout << "Unknown Online tracking algorithm: " << OnlineTracker::algo_str[algo]<< endl;
		return;
	}

	if ( algo == OnlineTracker::ALGO_CNN )
	{
		timer = clock();
	}
	tr_obj->_ok[algo] = tr->update( tr_obj->_frame, tr_obj->_loc[algo] );
	if ( algo == OnlineTracker::ALGO_CNN )
	{
		timer = clock() - timer;
		duration = 1000 * timer / CLOCKS_PER_SEC;
		if ( duration > DR_ALGO_TIMER_MAX_MS  &&
			( tr_obj->_ok[algo]  && ( tr_obj->_loc[algo].area() > 0.5 * tr_obj->rows() * tr_obj->cols()) )
		   )
		{
			cout<<  "  "<< OnlineTracker::algo_str[algo]<< ", exec time: "<< duration;
			turned_off = true;
			turned_off_frame_count = 0;
		}
	}

	if ( tr_obj->_ok[algo] )
	{
		cv::rectangle( tr_obj->_frame, tr_obj->_loc[algo], OnlineTracker::col[algo], 1, LINE_AA );
	}
	else
	{
	}
}


void OnlineTracker::init( Mat &frame, cv::Rect loc, bool initAll )
{
	Rect2d cnn;

	if ( (!initAll) &&  (_loc.size() == DR_NUM_ALGO)  )
	{
		cnn = _loc[ALGO_CNN];
	}

	_loc = vector<Rect2d>( DR_NUM_ALGO, loc );
	_ok = vector<bool>( DR_NUM_ALGO, true);
	_frame = frame;

	OnlineTracker::kcftr = TrackerKCF::create();
	OnlineTracker::miltr = TrackerMIL::create();

	kcftr->init( frame, _loc[ALGO_KCF]);
	miltr->init( frame, _loc[ALGO_MIL]);
	if ( initAll )
	{
		OnlineTracker::cnntr = TrackerGOTURN::create();
		cnntr->init( frame, _loc[ALGO_CNN]);
	}
	else
	{
		cv::Point pt( loc.tl() );
		if ( cv::norm( pt - cv::Point( cnn.tl() ) ) > loc.width )
		{
			OnlineTracker::cnntr = TrackerGOTURN::create();
			cnntr->init( frame, _loc[ALGO_CNN]);
		}
		else
		{
			runTracker( ALGO_CNN, this );
		}
	}
}

bool OnlineTracker::reInit( Algo_T algo, cv::Rect2d r )
{
	Ptr<Tracker> savedtr;
	bool okay;

	switch ( algo )
	{
	case ALGO_KCF:
		savedtr = OnlineTracker::kcftr;
		OnlineTracker::kcftr = TrackerKCF::create();
		okay = kcftr->init( _frame, r );
		if ( !okay )
		{
			OnlineTracker::kcftr = savedtr;
		}
		break;

	case ALGO_MIL:
		savedtr = OnlineTracker::miltr;
		OnlineTracker::miltr = TrackerMIL::create();
		okay = miltr->init( _frame, r );
		if ( !okay )
		{
			OnlineTracker::miltr = savedtr;
		}
		break;

	case ALGO_CNN:
		savedtr = OnlineTracker::cnntr;
		OnlineTracker::cnntr = TrackerGOTURN::create();
		okay = cnntr->init( _frame, r );
		if ( !okay )
		{
			OnlineTracker::cnntr = savedtr;
		}
		break;
	default:
		okay = false;
	}

	return okay;
}

bool OnlineTracker::track( Mat &frame)
{
	_frame = frame;

	std::thread thr[DR_NUM_ALGO];

	thr[ALGO_KCF] = std::thread( runTracker, ALGO_KCF, this );
	thr[ALGO_MIL] = std::thread( runTracker, ALGO_MIL, this );
	thr[ALGO_CNN] = std::thread( runTracker, ALGO_CNN, this );

	for ( size_t i = 0; i < DR_NUM_ALGO; ++i )
	{
		thr[i].join();
	}

	if ( turned_off  &&  (turned_off_frame_count == 0) )
	{
		if (_ok[ALGO_CNN]  &&  _ok[ALGO_MIL]  &&  (_loc[ALGO_CNN].area() < 2 * _loc[ALGO_MIL].area()) )
		{
			turned_off = false;
			cout<< endl;
		}
		else
		{
			cout<< "  Turning off "<< endl;
		}
	}

	bool b = false;
	for ( size_t i = 0; i < DR_NUM_ALGO; ++i )
	{
		b = ( b || _ok[i] );
	}

	return b;
}

Rect2d OnlineTracker::consensus()
{
	if ( _ok[ALGO_KCF] )
	{
		_consensus_loc = _loc[ALGO_KCF];
	}
	else if ( _ok[ALGO_MIL] )
	{
		_consensus_loc = _loc[ALGO_MIL];
	}
	else if ( _ok[ALGO_CNN] )
	{
		_consensus_loc = _loc[ALGO_CNN];
	}

	if ( !_ok[ALGO_KCF]  &&  _ok[ALGO_MIL]  )
	{
		reInit( ALGO_KCF, _loc[ALGO_MIL] );
	}

	if ( _ok[ALGO_CNN]  &&  _ok[ALGO_MIL]  &&  ( _loc[ALGO_CNN].area() < _loc[ALGO_MIL].area() ) )
	{
		cv::Point cnn_centre = OnlineTracker::centre( _loc[ALGO_CNN] );
		if ( !(_loc[ALGO_MIL].contains( cnn_centre ) ) )
		{
			int w = (_loc[ALGO_MIL].width > _loc[ALGO_CNN].width) ?
					_loc[ALGO_MIL].width : _loc[ALGO_CNN].width + 4;
			int h = (_loc[ALGO_MIL].height > _loc[ALGO_CNN].height) ?
					_loc[ALGO_MIL].height : _loc[ALGO_CNN].height + 4;
			int x = cnn_centre.x - 0.5 * w;
			int y = cnn_centre.y - 0.5 * h;
			x = ( x < 0 ) ? 0 : x;
			y = ( y < 0 ) ? 0 : y;
			w = ( w > (int)_frame.cols ) ? _frame.cols : w;
			h = ( h > (int)_frame.rows ) ? _frame.rows : h;
			Rect2d rec = cv::Rect2d( x, y, w, h );

			reInit( ALGO_MIL, rec );
			reInit( ALGO_KCF, rec );
		}
	}

	/*
	if ( _ok[ALGO_MIL]  &&  ( _loc[ALGO_CNN].area() > 1.5 * _loc[ALGO_MIL].area()) )
	{
		OnlineTracker::cnntr = TrackerGOTURN::create();
		cnntr->init( _frame, _loc[ALGO_MIL]);
	}*/

	/*
	if ( _ok[ALGO_CNN] )
	{
		cv::Point cnn_centre( _loc[ALGO_CNN].x + 0.5 * _loc[ALGO_CNN].width,
					  	  	  _loc[ALGO_CNN].y + 0.5 * _loc[ALGO_CNN].height );
		if ( _ok[ALGO_KCF] )
		{
			if ( !_loc[ALGO_KCF].contains( cnn_centre ) )
			{
				OnlineTracker::cnntr = TrackerGOTURN::create();
				cnntr->init( _frame, _loc[ALGO_KCF]);
			}
		}
		else if (_ok[ALGO_MIL] )
		{
			if ( !_loc[ALGO_MIL].contains( cnn_centre ) )
			{
				OnlineTracker::cnntr = TrackerGOTURN::create();
				cnntr->init( _frame, _loc[ALGO_MIL]);
			}
		}
	}*/


	return _consensus_loc;
}
