#include <fcgiapp.h>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string/replace.hpp>
#include <stdlib.h>
#include <time.h>

#include <vision_def.h>
#include <drone_vision.h>
#include <dronedb.h>
#include "qhandler.h"


using namespace std;



int main()
{
	FCGX_Stream *in, *out, *err;
	FCGX_ParamArray envp;


	srand(time(NULL));

	while (FCGX_Accept(&in, &out, &err, &envp) >= 0)
	{
		std::string response;

		char *q = FCGX_GetParam("QUERY_STRING", envp);
		if (!q)
		{
			response = "FCGX_GetParam failed. No query string: check web server configuration\n";
		}
		else
		{
			response = QueryHandler::handle_query( q );

		}
		FCGX_FPrintF(out, response.c_str() );
	}
	return 0;
}
